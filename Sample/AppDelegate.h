//
//  AppDelegate.h
//  Sample
//
//  Created by Ethan on 2014/11/23.
//  Copyright (c) 2014年 Tutorial. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

