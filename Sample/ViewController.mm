//
//  ViewController.m
//  Sample
//
//  Created by Ethan on 2014/11/23.
//  Copyright (c) 2014年 Tutorial. All rights reserved.
//

#import "ViewController.h"
#import "Novocaine.h"
#include <Accelerate/Accelerate.h>
#include <vector>
#include <map>
#include <algorithm>
#include <numeric>
#include <mach/mach.h>
#include <mach/mach_time.h>
#import "DrawViewController.h"
#import "RawDrawViewController.h"


@interface ViewController ()

@property(strong, nonatomic) Novocaine * audioManager;
@property(weak, nonatomic) IBOutlet UILabel* outputLbl;
@property(weak, nonatomic) IBOutlet UITextField* freqTF;
@property(weak, nonatomic) IBOutlet UITextField* volTF;
@property(weak, nonatomic) IBOutlet UIView* colorV;
@property(weak, nonatomic) IBOutlet UILabel* hrLbl;
@property(weak, nonatomic) IBOutlet UILabel* secLbl;
@property(strong, nonatomic) NSTimer * timer;
@property(nonatomic) int second;
@property(nonatomic) float freq;
@property(nonatomic) float volume;
@property(strong, nonatomic) NSMutableArray * diffArr;
@property(strong, nonatomic) NSMutableArray * nnArr;
@property(strong, nonatomic) DrawViewController * drawV;
@property(strong, nonatomic) RawDrawViewController * rawDrawV;

@end

@implementation ViewController

#define LEFT_CHANNEL 1
#define RIGHT_CHANNEL 0

#define WINDOW 250
#define SAMPLE_RATE 48000

#define INTERVAL 64
#define WIDTH 4

struct Stat{
    float stdrr;
    float vlf;
    float lf;
    float hf;
};

int findFirstPeak(std::vector<float>& dataVec, int& start, std::vector<float>& maxVec){
    float minVal;
    float maxVal;
    unsigned long minValIdx;
    unsigned long maxValIdx;
    unsigned long maxShift = 0;
    unsigned long minShift = 0;
    while(start + WINDOW < dataVec.size()){
        vDSP_minvi(&dataVec[start], 1, &minVal, &minValIdx, WINDOW);
        vDSP_maxvi(&dataVec[start], 1, &maxVal, &maxValIdx, WINDOW);
//        // max part
//        if(maxValIdx == WINDOW / 2){
//            // it's a max peak
//            NSLog(@"MaxPeak %f", maxVal);
//            return start + WINDOW / 2;
//        } else if(maxValIdx > WINDOW / 2){
//            maxShift = maxValIdx - WINDOW / 2;
//        } else{
//            maxShift = 0;
//        }
        
        // min part
        if(minValIdx == WINDOW / 2){
            // it's a min peak
            NSLog(@"MinPeak %f",(maxVal - minVal));
            return start + WINDOW / 2;
        } else if(minValIdx > WINDOW / 2){
            minShift = minValIdx - WINDOW / 2;
        } else {
            minShift = 0;
        }

        if(minShift == 0 && maxShift == 0){
            start += WINDOW / 2;
        } else if(minShift != 0 && maxShift != 0){
            // shift small one
            start += ( maxShift > minShift ? minShift : maxShift );
        } else if(minShift != 0){
            start += minShift;
        } else{
            start += maxShift;
        }
    }
    return -1;
}

- (void)stop{
    [_audioManager pause];
}

- (void)start{
}

void doAnalysis(std::vector<float> data, Stat& stat){
    // time
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);
        double mean = sum / data.size();
        
        double sq_sum = std::inner_product(data.begin(), data.end(), data.begin(), 0.0);
        stat.stdrr = std::sqrt(sq_sum / data.size() - mean * mean);
    }

    // freq
    {
        const float FFT_SAMPLE_RATE = 4.;
        const int FFT_SAMPLE_COUNT = 512;
        const float log2n = 9;
        
        // interpolation
        std::vector<float> rrSignal(FFT_SAMPLE_COUNT);
        
        float currentX = 0.;
        std::vector<float>::const_iterator left = data.begin();
        std::vector<float>::const_iterator right = left + 1;
        for(int i = 0 ; i < FFT_SAMPLE_COUNT ; i ++){
            float x = i * 250.;
            // make sure : left <= x <= right
            while (x > (*right + currentX)) {
                currentX += *left;
                ++left;
                ++right;
            }
            
            // y - left_y / x - left_x = (right_y - left_y) / ( right_x - left_x)
            rrSignal[i] = (*right - *left) * (x - currentX) / *right + *left;
        }
        
        FFTSetup specAnalysis = vDSP_create_fftsetup(log2n, FFT_RADIX2);
        DSPSplitComplex cmplx;
        cmplx.realp = new float[FFT_SAMPLE_COUNT / 2];
        cmplx.imagp = new float[FFT_SAMPLE_COUNT / 2];
        memset(cmplx.realp, 0, FFT_SAMPLE_COUNT * sizeof(float) / 2);
        memset(cmplx.imagp, 0, FFT_SAMPLE_COUNT * sizeof(float) / 2);
        
        std::vector<float> window(FFT_SAMPLE_COUNT);
        vDSP_hamm_window(&window[0], FFT_SAMPLE_COUNT, 0);

        float mean = 0;
        vDSP_meanv ( &rrSignal[0], 1, &mean, FFT_SAMPLE_COUNT );
        
        for(int i = 0 ; i < FFT_SAMPLE_COUNT ; i++ ){
            rrSignal[i] = (rrSignal[i] - mean) * window[i];
        }
        
        vDSP_ctoz((COMPLEX *) &rrSignal[0], 2, &cmplx, 1, FFT_SAMPLE_COUNT / 2);
        vDSP_fft_zrip(specAnalysis, &cmplx, 1, log2n, FFT_FORWARD);
        
        stat.vlf = stat.lf = stat.hf = 0.;
        
        for(int i = 0 ; i < FFT_SAMPLE_COUNT / 2 ; i ++){
            float freq = 1. * i * FFT_SAMPLE_RATE / FFT_SAMPLE_COUNT;
            float mag = (cmplx.realp[i]*cmplx.realp[i]+ cmplx.imagp[i]*cmplx.imagp[i])/4;
            if(freq < 0.04){
                stat.vlf += mag;
            } else if(freq < 0.15){
                stat.lf += mag;
            } else if(freq < 0.4){
                stat.hf += mag;
            } else{
                break;
            }
        }
        const float HAMM_FACTOR=1.586;
        stat.vlf = stat.vlf * HAMM_FACTOR / (FFT_SAMPLE_COUNT * FFT_SAMPLE_COUNT / 2);
        stat.lf = stat.lf * HAMM_FACTOR / (FFT_SAMPLE_COUNT * FFT_SAMPLE_COUNT / 2);
        stat.hf = stat.hf * HAMM_FACTOR / (FFT_SAMPLE_COUNT * FFT_SAMPLE_COUNT / 2);
    }
}

-(void)uv{
    if(_second == 0){
        [self stop];
        
        std::vector<float> tmpDiffArr(_diffArr.count);
        for(int i = 0 ; i < _diffArr.count ; i++){
            tmpDiffArr[i] = ((NSNumber*)[_diffArr objectAtIndex:i]).floatValue;
        }
        
        Stat stat;

        doAnalysis(tmpDiffArr, stat);
        
        _secLbl.text = [NSString stringWithFormat:@"STDRR:%.2f\tVLF:%.2f\tLF:%.2f\tHF:%.2f\tLF/HF:%.2f",stat.stdrr,stat.vlf, stat.lf, stat.hf, stat.lf/stat.hf];
        [_timer invalidate];
        _timer = nil;
    } else {
        _secLbl.text = [NSString stringWithFormat:@"%d sec",_second--];
    }
}

-(IBAction)stopBtn{
    
}

-(IBAction)startBtn{
    _second = 130;
    _diffArr = [[NSMutableArray alloc] init];
    _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(uv) userInfo:nil repeats:YES];
    [self start];
    _volume = _volTF.text.floatValue;
    [_volTF endEditing:YES];
}

#define RAND static_cast <float> (rand()) / static_cast <float> (RAND_MAX)

- (void)viewDidLoad {
    [super viewDidLoad];
    __weak ViewController * wSelf = self;
    _freq = 4800;
    _volume = 0.85;
    _freqTF.text = [NSString stringWithFormat:@"%.0f",_freq];
    _volTF.text = [NSString stringWithFormat:@"%.1f",_volume];
    _audioManager = [Novocaine audioManager];
    
    _drawV = [[DrawViewController alloc] init];
    [graphV addSubview:_drawV.view];
    [_drawV.view setFrame:CGRectMake(0, 0, graphV.bounds.size.width, graphV.bounds.size.height)];

//    _rawDrawV = [[RawDrawViewController alloc] init];
//    [graphV addSubview:_rawDrawV.view];
//    [_rawDrawV.view setFrame:CGRectMake(0, graphV.bounds.size.height / 2, graphV.bounds.size.width, graphV.bounds.size.height / 2)];
    
    __block std::vector<float> buffer;
    buffer.reserve(SAMPLE_RATE * 2);
    __block std::vector<float> dataToInsert;
    __block std::vector<unsigned long> countArray(INTERVAL);
    memset(&countArray[0], 0, sizeof(unsigned long) * INTERVAL);
    __block unsigned long maxIdx = 99999, maxCount = 0;
    __block int start = 0;
    __block int counter = 5;
    __block std::vector<int> diff;
    diff.reserve(10);
    __block std::vector<float> maxVec;
    maxVec.reserve(10);
    _second = -1;
    __block int rawCount = 0;
    [_audioManager setInputBlock:^(float *newAudio, UInt32 numSamples, UInt32 numChannels) {
        // find min
        float val;
        unsigned long idx;
        
//        if(++rawCount % 10 == 0){
//            rawCount = 0;
//            [wSelf.rawDrawV setData:newAudio WithLen:numSamples];
//        }
        
        vDSP_minvi(newAudio, 1, &val, &idx, numSamples);
        
        unsigned long residus = idx % INTERVAL;
        countArray[residus] += 1;
        if(countArray[residus] > maxCount){
            maxIdx = residus;
            maxCount = countArray[residus];
        }
        
        int ret = findFirstPeak(buffer, start, maxVec);
        for(int i = 0 ; i < numSamples ; i += INTERVAL){
            [wSelf.drawV insertData:newAudio[i + maxIdx]];
//            [wSelf.rawDrawV insertData:newAudio[i + maxIdx]];
            buffer.push_back(newAudio[i+maxIdx]);
        }
        if(ret >= 0){
            diff.push_back(ret);
            float diffInMs = 1000. * INTERVAL * ret / SAMPLE_RATE ;
            [wSelf.diffArr addObject:[NSNumber numberWithFloat:diffInMs]];

            if(diffInMs ){
                [wSelf.nnArr addObject:[NSNumber numberWithFloat:diffInMs]];
            }
            
            if(diff.size() > 10){
                diff.erase(diff.begin());
            }
            dispatch_async(dispatch_get_main_queue(), ^(){
                if(diff.size() == 10){
                    float avg = 0.;
                    NSMutableString * s = [[NSMutableString alloc] init];
                    for(std::vector<float>::const_iterator it = maxVec.begin();
                        it != maxVec.end();
                        ++it){
                        [s appendFormat:@"%d\t",(int)(10000*(*it))];
                    }
                    for(std::vector<int>::const_iterator it = diff.begin();
                        it != diff.end();
                        ++it){
                        avg += (1. * (*it) * INTERVAL / SAMPLE_RATE);
                    }
                    if(wSelf.second == -1)
                        wSelf.secLbl.text = s;
                    wSelf.hrLbl.text = [NSString stringWithFormat:@"%.0f bpm", 60 / (avg / 10)];
                }
                wSelf.colorV.backgroundColor = [UIColor redColor];
            });
            counter = 5;
            buffer.erase(buffer.begin(), buffer.begin() + ret);
            start = 0;
        }
        if(counter-- < 0){
            dispatch_async(dispatch_get_main_queue(), ^(){
                wSelf.colorV.backgroundColor = [UIColor whiteColor];
            });
        }
    }];
    [_audioManager setOutputBlock:^(float *audioToPlay, UInt32 numSamples, UInt32 numChannels) {
        for(int i = 0 ; i < numSamples ; i += INTERVAL){
            for(int j = 0 ; j < INTERVAL ; j++){
                if(j < WIDTH){
                    audioToPlay[numChannels*(i+j)+LEFT_CHANNEL] = -1. * wSelf.volume;
                    audioToPlay[numChannels*(i+j)+RIGHT_CHANNEL] = 0;
                } else if(j < 2 * WIDTH){
                    audioToPlay[numChannels*(i+j)+LEFT_CHANNEL] = 1. * wSelf.volume;
                    audioToPlay[numChannels*(i+j)+RIGHT_CHANNEL] = 0;
                } else{
                    audioToPlay[numChannels*(i+j)+LEFT_CHANNEL] = 0;
                    audioToPlay[numChannels*(i+j)+RIGHT_CHANNEL] = 0;
                }
            }
        }
    }];
    [_audioManager play];
}

- (void)dealloc{
    [_audioManager pause];
    _audioManager = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
