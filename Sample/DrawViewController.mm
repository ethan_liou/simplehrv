//
//  DrawViewController.m
//  SimpleHR
//
//  Created by Ethan on 2015/2/12.
//  Copyright (c) 2015年 Tutorial. All rights reserved.
//

#import "DrawViewController.h"
#import <Accelerate/Accelerate.h>

@implementation DrawViewController

- (id)init{
    self = [super initWithNibName:nil bundle:nil];
    if(self){
        effect = [[GLKBaseEffect alloc] init];
        GLKView * view = (GLKView *)self.view;
        view.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
        self.delegate = self;
        
        buffer = new GLfloat[4096*2];
        tempBuffer = new float[4096];
        
        // Let's color the line
        effect.useConstantColor = GL_TRUE;
        
        // Make the line a cyan color
        effect.constantColor = GLKVector4Make(
                                               0.0f, // Red
                                               1.0f, // Green
                                               0.0f, // Blue
                                               1.0f);// Alpha

    }
    return self;
}

-(void)dealloc{
    delete buffer; buffer = NULL;
    delete tempBuffer; tempBuffer = NULL;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect {
    glClearColor(0.f, 0.f, 0.f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    // Prepare the effect for rendering
    [effect prepareToDraw];
    
    // Create an handle for a buffer object array
    GLuint bufferObjectNameArray;
    
    // Have OpenGL generate a buffer name and store it in the buffer object array
    glGenBuffers(1, &bufferObjectNameArray);
    
    // Bind the buffer object array to the GL_ARRAY_BUFFER target buffer
    glBindBuffer(GL_ARRAY_BUFFER, bufferObjectNameArray);
    // Send the line data over to the target buffer in GPU RAM
    glBufferData(
                 GL_ARRAY_BUFFER,   // the target buffer
                 4096 * 2 * sizeof(GLfloat),       // the number of bytes to put into the buffer
                 buffer,           // a pointer to the data being copied
                 GL_STATIC_DRAW);   // the usage pattern of the data
    
    // Enable vertex data to be fed down the graphics pipeline to be drawn
    glEnableVertexAttribArray(GLKVertexAttribPosition);
    
    // Specify how the GPU looks up the data
    glVertexAttribPointer(
                          GLKVertexAttribPosition, // the currently bound buffer holds the data
                          2,                       // number of coordinates per vertex
                          GL_FLOAT,                // the data type of each component
                          GL_FALSE,                // can the data be scaled
                          2*4,                     // how many bytes per vertex (2 floats per vertex)
                          NULL);                   // offset to the first coordinate, in this case 0
    
    glLineWidth(2.0f);
    
    glDrawArrays(GL_LINE_STRIP, 0, 4096); // render
    
    glDeleteBuffers(1, &bufferObjectNameArray);
}

-(void)insertData:(float)data{
    memmove(tempBuffer, tempBuffer+1, (4096-1)*sizeof(float));
    tempBuffer[4096-1] = data;
}

- (void)glkViewControllerUpdate:(GLKViewController *)controller{
    Float32 M=1,m=0;
    vDSP_maxv(tempBuffer, 1, &M, 4096);
    vDSP_minv(tempBuffer, 1, &m, 4096);
    
    for(int i = 0 ; i < 4096 ; i ++){
        buffer[2 * i] = -1. + 2. * i / 4096;
        buffer[2 * i + 1] = -1. + 2. * (tempBuffer[i] - m) / (M-m);
    }
}

@end
